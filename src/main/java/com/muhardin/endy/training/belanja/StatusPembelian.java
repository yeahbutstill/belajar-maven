package com.muhardin.endy.training.belanja;

public enum StatusPembelian {
    BELUM_DIBAYAR,
    LUNAS,
    SEDANG_DIKIRIM,
    DITERIMA,
    KOMPLAIN,
    SELESAI
}
