package com.muhardin.endy.training.belanja.db;

import com.muhardin.endy.training.belanja.Produk;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ProdukDb {
    private static final String NAMA_FILE_PRODUK = "produk.txt";

    private List<Produk> dataProduk = new ArrayList<>();

    public ProdukDb() {
        try {
            Charset charset = Charset.forName("UTF-8");
            Path lokasiFile = Path.of(ClassLoader.getSystemResource(NAMA_FILE_PRODUK).toURI());
            BufferedReader reader = Files.newBufferedReader(lokasiFile, charset);
            String line =
                    reader.readLine(); // baca baris pertama, lalu diabaikan karena isinya header saja
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                String[] data = line.split(",");

                Produk p = new Produk();
                p.setMerek(data[0]);
                p.setTipe(data[1]);
                p.setDeskripsi(data[2]);
                p.setHarga(new BigDecimal(data[3]));
                dataProduk.add(p);
            }
            reader.close();
        } catch (IOException err) {
            System.out.println("Terjadi error membaca data : " + err.getMessage());
        } catch (URISyntaxException err) {
            System.out.println("Nama file salah : " + err.getMessage());
        }
    }

    public List<Produk> semuaProduk() {
        return dataProduk;
    }

    public List<Produk> cariProdukByMerek(String merek) {
        return null;
    }
}
