package com.muhardin.endy.training.belanja;

import java.math.BigDecimal;

public class DiskonNilai implements Diskon {

    private static final BigDecimal BATAS_DISKON = new BigDecimal(15000000);
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.1);

    @Override
    public BigDecimal hitung(Pembelian p) {
        if (BATAS_DISKON.compareTo(p.total()) < 0) {
            return PERSENTASE_DISKON.multiply(p.total());
        }
        return BigDecimal.ZERO;
    }
}
