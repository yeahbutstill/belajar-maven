package com.muhardin.endy.training.belanja;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DetailPembelian {
    private Produk produk;
    private Integer jumlah;

    public BigDecimal subtotal() {
        return produk.getHarga().multiply(new BigDecimal(jumlah));
    }
}
