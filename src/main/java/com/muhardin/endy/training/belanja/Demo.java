package com.muhardin.endy.training.belanja;

import com.muhardin.endy.training.belanja.db.PembelianDb;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

public class Demo {
    public static void main(String[] abc) {
        Produk iphone10 = new Produk();
        iphone10.setMerek("Apple");
        iphone10.setTipe("iPhone 10");
        iphone10.setHarga(new BigDecimal(-1500000));

        Produk zfold4 = new Produk("Samsung", "Z Fold4", new BigDecimal(2499900));

        System.out.println("=== Informasi Produk ===");
        System.out.println("Merek : " + zfold4.getMerek());
        System.out.println("Tipe : " + zfold4.getTipe());
        System.out.println("Harga : " + zfold4.getHarga());

        Pembelian transaksi = new Pembelian();

        // beli iphone 3 unit
        DetailPembelian dp1 = new DetailPembelian();
        dp1.setProduk(iphone10);
        dp1.setJumlah(3);
        System.out.println("Subtotal iPhone : " + dp1.subtotal());

        // beli zfold 2 unit
        DetailPembelian dp2 = new DetailPembelian();
        dp2.setProduk(zfold4);
        dp2.setJumlah(2);
        System.out.println("Subtotal Samsung : " + dp2.subtotal());

        transaksi.getDaftarDetailPembelian().add(dp1);
        transaksi.getDaftarDetailPembelian().add(dp2);

        // DecimalFormat formatter = new DecimalFormat("$###,###.###");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("id", "id"));

        BigDecimal totalNilaiTransaksi = transaksi.total();
        System.out.println("Nilai total transaksi : " + formatter.format(totalNilaiTransaksi));

        Diskon diskonNilai = new DiskonNilai();
        BigDecimal potongan = diskonNilai.hitung(transaksi);
        System.out.println("Nilai diskon total : " + potongan.setScale(2, RoundingMode.HALF_EVEN));

        Diskon diskonProduk = new DiskonProduk();
        BigDecimal potonganProduk = diskonProduk.hitung(transaksi);
        System.out.println(
                "Nilai diskon produk : " + potonganProduk.setScale(2, RoundingMode.HALF_EVEN));

        PembelianDb pembelianDb = new PembelianDb();
        pembelianDb.simpan(transaksi);
    }
}
