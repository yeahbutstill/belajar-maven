package com.muhardin.endy.training.belajar.toko.dao;

import com.muhardin.endy.training.belajar.toko.entity.Produk;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class ProdukDb {
    private static final String NAMA_FILE = "produk_laptop.txt";
    // disini kita baca sekali saja diawal, kemudian nanti kita masukan ke variable di internal
    private List<Produk> dataProduk = new ArrayList<>();

    public ProdukDb() {
        Charset charset = StandardCharsets.UTF_8;
        URL urlFile = ClassLoader.getSystemResource(NAMA_FILE);
        // check apakah file ada atau tidak
        if (urlFile == null) {
            System.out.println("File tidak ditemukan");
            // ini selesai constructornya tidak lanjut membaca file, sehingga dataProduk kosong
            return;
        }

        Path pathDbProduk = null;
        try {
            pathDbProduk = Path.of(urlFile.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        // check ukuran file, balikannya byte
        try {
            System.out.println("Ukuran file database: " + Files.size(pathDbProduk));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        BasicFileAttributes attr = null;
        try {
            attr = Files.readAttributes(pathDbProduk, BasicFileAttributes.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Waktu pembuatan: " + attr.creationTime());
        System.out.println("Terakhir dibuka: " + attr.lastAccessTime());
        System.out.println("Terakhir diedit: " + attr.lastModifiedTime());

        try (BufferedReader reader = Files.newBufferedReader(pathDbProduk, charset)) {
            // dibaca dulu
            String line = reader.readLine();
            // kemudian kita skip, langsung baca lagi
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                String[] data = line.split(",");
                Produk p = new Produk();
                p.setCode(data[0]);
                p.setName(data[1]);
                p.setFoto(data[2]);
                p.setPrice(new BigDecimal(data[3]));
                dataProduk.add(p);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Produk> semuaProduk() {
        return dataProduk;
    }

    public Produk cariByCode(String code) {
        for (Produk produk : dataProduk) {
            if (code.equalsIgnoreCase(produk.getCode())) {
                return produk;
            }
        }
        return null;
    }

    /**
     * implement save produk
     */
    public void save() {
        Charset charset = StandardCharsets.UTF_8;
        Path pathDbProduk = null;
        try {
            pathDbProduk = Path.of(ClassLoader.getSystemResource(NAMA_FILE).toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        try (BufferedWriter writer = Files.newBufferedWriter(pathDbProduk, charset)) {
            String s = "code,name,foto,price";
            writer.write(s, 0, s.length());
            for (Produk produk : dataProduk) {
                s = "\r\n";
                s += produk.getCode();
                s += ",";
                s += produk.getName();
                s += ",";
                s += produk.getFoto();
                s += ",";
                // karena Price adalah balikannya BigDecimal kita setScale supaya tidak usah pake angka
                // dibelakang koma
                s += produk.getPrice().setScale(0, RoundingMode.HALF_EVEN);
                writer.write(s, 0, s.length());
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }

    /**
     * implement hapus data berdasarkan index
     * @param index
     */
    public void hapus(int index) {
        // list produk lalu remove perdasarkan index
        dataProduk.remove(index);
        // setelah itu save
        save();
    }
}
