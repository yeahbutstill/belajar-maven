package com.muhardin.endy.training.belajar.threaddemo;

import java.util.ArrayList;
import java.util.List;

public class AtomicDemo {
    public static void main(String[] args) throws InterruptedException {
        Integer jumlahThread = 1000000;
        Integer jumlahUlang = 10;
        AtomicCounter counter = new AtomicCounter();

        List<TambahCounter> daftarThreadTambah = new ArrayList<>();
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadTambah.add(new TambahCounter(counter, jumlahUlang));
        }

        List<KurangCounter> daftarThreadKurang = new ArrayList<>();
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadKurang.add(new KurangCounter(counter, jumlahUlang));
        }

        // start semua thread
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadTambah.get(i).start();
            daftarThreadKurang.get(i).start();
        }

        // join semua thread
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadTambah.get(i).join();
            daftarThreadKurang.get(i).join();
        }

        // tampilkan
        System.out.println("========= Yang paling akhir ==========");
        counter.show();
    }

    static class TambahCounter extends Thread {
        private Integer ulang;
        private AtomicCounter counter;

        private TambahCounter(AtomicCounter c, Integer u) {
            this.counter = c;
            this.ulang = u;
        }

        @Override
        public void run() {
            for (int i = 0; i < ulang; i++) {
                counter.tambah();
            }
        }
    }

    static class KurangCounter extends Thread {
        private Integer ulang;
        private AtomicCounter counter;

        private KurangCounter(AtomicCounter c, Integer u) {
            this.counter = c;
            this.ulang = u;
        }

        @Override
        public void run() {
            for (int i = 0; i < ulang; i++) {
                counter.kurang();
            }
        }
    }
}
