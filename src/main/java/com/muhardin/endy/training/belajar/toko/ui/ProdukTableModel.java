package com.muhardin.endy.training.belajar.toko.ui;

import com.muhardin.endy.training.belajar.toko.entity.Produk;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ProdukTableModel extends AbstractTableModel {
    private List<Produk> dataProduk;

    public ProdukTableModel(List<Produk> dataProduk) {
        this.dataProduk = dataProduk;
    }

    @Override
    public int getRowCount() {
        return dataProduk.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        return switch (column) {
            case 0 -> "Kode";
            case 1 -> "Nama";
            case 2 -> "Harga";
            default -> "";
        };
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Produk p = dataProduk.get(rowIndex);
        return switch (columnIndex) {
            case 0 -> p.getCode();
            case 1 -> p.getName();
            case 2 -> FormatAngkaHelper.formatAngkaUang(p.getPrice());
            default -> "";
        };
    }
}
