package com.muhardin.endy.training.belajar.toko.service.impl;

import com.muhardin.endy.training.belajar.toko.entity.Customer;
import com.muhardin.endy.training.belajar.toko.entity.Pembelian;
import com.muhardin.endy.training.belajar.toko.service.Diskon;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class DiskonCustomer implements Diskon {
    private static final List<String> NAMA_DISKON = Arrays.asList("agus", "agustina");
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.05);

    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        Customer c = pembelian.getCustomer();
        String nama = c.getName();
        if (NAMA_DISKON.contains(nama.trim().toLowerCase())) {
            return PERSENTASE_DISKON.multiply(pembelian.total());
        }

        // else
        return BigDecimal.ZERO;
    }
}
