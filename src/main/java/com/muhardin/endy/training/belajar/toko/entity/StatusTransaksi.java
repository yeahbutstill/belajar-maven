package com.muhardin.endy.training.belajar.toko.entity;

public enum StatusTransaksi {
    BELUM_DIBAYAR,
    LUNAS,
    DIPROSES,
    DALAM_PENGIRIMAN,
    DITERIMA,
    SELESAI,
    KOMPLAIN
}
