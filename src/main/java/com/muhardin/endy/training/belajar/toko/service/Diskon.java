package com.muhardin.endy.training.belajar.toko.service;

import com.muhardin.endy.training.belajar.toko.entity.Pembelian;

import java.math.BigDecimal;

public interface Diskon {
    // ini dibacanya bahwa method hitung ini inputannya hitung, outputnya bigdecimal
    BigDecimal hitung(
            Pembelian pembelian); // abstract method (tidak ada implementasinya, hanya deklarasi saja)

    // 1. Diskon dari total pembelian
    // 2. Diskon untuk produk tertentu
    // 3. Diskon untuk customer yang namanya agus
    // 4. Diskon gajian, 3 hari terakhir setiap bulan
}
