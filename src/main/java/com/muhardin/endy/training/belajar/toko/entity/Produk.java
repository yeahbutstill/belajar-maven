package com.muhardin.endy.training.belajar.toko.entity;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Produk {
    private String code;
    private String name;
    private String foto;
    private BigDecimal price;
}
