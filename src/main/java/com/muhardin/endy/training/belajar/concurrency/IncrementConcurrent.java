package com.muhardin.endy.training.belajar.concurrency;

public class IncrementConcurrent extends Thread {
    private String nama;

    public IncrementConcurrent(String nama) {
        this.nama = nama;
    }

    public void run() {
        Integer max = 5;

        for (int i = 0; i < max; i++) {
            try {
                System.out.println(nama + " - i : " + i);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
