package com.muhardin.endy.training.belajar.toko.service.impl;

import com.muhardin.endy.training.belajar.toko.entity.Pembelian;
import com.muhardin.endy.training.belajar.toko.entity.PembelianDetail;
import com.muhardin.endy.training.belajar.toko.entity.Produk;
import com.muhardin.endy.training.belajar.toko.service.Diskon;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class DiskonProduk implements Diskon {
    private static final List<String> DAFTAR_PRODUCT_DISKON = Arrays.asList("P-001", "P-002");
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.15); // 15%

    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        BigDecimal hasil = BigDecimal.ZERO;
        for (PembelianDetail pDetail : pembelian.getDaftarPembelianDetail()) {
            Produk p = pDetail.getProduk();
            if (DAFTAR_PRODUCT_DISKON.contains(p.getCode())) {
                hasil = hasil.add(PERSENTASE_DISKON.multiply(pDetail.subTotal()));
            }
        }
        return hasil;
    }
}
