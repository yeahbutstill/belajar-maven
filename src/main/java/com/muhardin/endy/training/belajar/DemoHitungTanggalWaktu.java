package com.muhardin.endy.training.belajar;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DemoHitungTanggalWaktu {
    public static void main(String[] args) {
        LocalDateTime sekarang = LocalDateTime.now();
        System.out.println("Waktu sekarang : " + sekarang);

        LocalDateTime setengahJamKemudian = sekarang.plusMinutes(30);
        System.out.println("Waktu 30 menit lagi : " + setengahJamKemudian);

        LocalDateTime sepuluhHariLagi = sekarang.plusDays(10);
        System.out.println("10 hari lagi, jatuh di hari " + sepuluhHariLagi.getDayOfWeek());

        String waktu = "2022-09-20 14:32:00";
        LocalDateTime tanggal20 =
                LocalDateTime.parse(waktu, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println("Tanggal 20 september 2022 jatuh di hari : " + tanggal20.getDayOfWeek());
    }
}
