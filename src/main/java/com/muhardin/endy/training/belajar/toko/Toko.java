package com.muhardin.endy.training.belajar.toko;

import com.muhardin.endy.training.belajar.toko.dao.ProdukDb;
import com.muhardin.endy.training.belajar.toko.entity.Customer;
import com.muhardin.endy.training.belajar.toko.entity.Pembelian;
import com.muhardin.endy.training.belajar.toko.entity.PembelianDetail;
import com.muhardin.endy.training.belajar.toko.entity.Produk;
import com.muhardin.endy.training.belajar.toko.service.impl.DiskonAkhirBulan;
import com.muhardin.endy.training.belajar.toko.service.impl.DiskonCustomer;
import com.muhardin.endy.training.belajar.toko.service.impl.DiskonProduk;
import com.muhardin.endy.training.belajar.toko.service.impl.DiskonTotal;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;

public class Toko {
    public static void main(String[] args) {
        System.out.println("Waktu saat ini: " + LocalDateTime.now());

        //    Produk p1 = new Produk();
        //    p1.setCode("P-001");
        //    p1.setName("Laptop Asus");
        //    p1.setFoto("p1.jpg");
        //    p1.setPrice(new BigDecimal(13000000));
        //
        //    Produk p2 = new Produk();
        //    p2.setCode("P-002");
        //    p2.setName("Laptop Acer");
        //    p2.setFoto("p2.jpg");
        //    p2.setPrice(new BigDecimal(10000000));
        //
        //    Produk p3 = new Produk();
        //    p3.setCode("P-003");
        //    p3.setName("Macbook Pro");
        //    p3.setFoto("p3.jpg");
        //    p3.setPrice(new BigDecimal(25000000));

        Customer c1 = new Customer();
        c1.setName("maya");
        c1.setEmail("maya@yeahbutstill.com");

        ProdukDb databaseProduk = new ProdukDb();
        Produk p1 = databaseProduk.cariByCode("P-001");
        Produk p3 = databaseProduk.cariByCode("P-003");

        PembelianDetail pd1 = new PembelianDetail(p1, 2);
        PembelianDetail pd2 = new PembelianDetail(p3, 1);

        Pembelian pb = new Pembelian();
        pb.setCustomer(c1);
        pb.getDaftarPembelianDetail().add(pd1);
        pb.getDaftarPembelianDetail().add(pd2);
        //    ord.getDaftarPembelianDetail().add(pd3);

        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.of("in", "id"));
        System.out.println("Pembelian oleh: " + pb.getCustomer().getName());
        System.out.println("================ Rincian =================");
        for (PembelianDetail pbd : pb.getDaftarPembelianDetail()) {
            System.out.println("Produk: " + pbd.getProduk().getName());
            System.out.println("Price: " + formatter.format(pbd.getProduk().getPrice().setScale(0)));
            System.out.println("Quantity: " + pbd.getJumlah());
            System.out.println("Subtotal: " + formatter.format(pbd.subTotal().setScale(0)));
            System.out.println("-----------------------------------------");
        }
        System.out.println("Total: Rp" + formatter.format(pb.total().setScale(0)));

        // aplikasikan diskon ke pembelian
        pb.getDaftarDiskon().add(new DiskonTotal());
        pb.getDaftarDiskon().add(new DiskonProduk());
        pb.getDaftarDiskon().add(new DiskonCustomer());
        pb.getDaftarDiskon().add(new DiskonAkhirBulan());
        System.out.println(
                "Diskon: " + formatter.format(pb.totalDiskon().setScale(0, RoundingMode.HALF_EVEN)));

        // menampilkan rincian diskon
        pb.tampilkanRincianDiskon();

        // menampilkan yang harus dibayar
        System.out.println(
                "Total Pembayaran: "
                        + formatter.format(pb.totalBayar().setScale(0, RoundingMode.HALF_EVEN)));

        System.out.println("---------------------------------------------");

        // mengambil tanggal terakhir di bulan ini
        LocalDateTime hariIni = LocalDateTime.now();
        LocalDateTime akhirBulan = hariIni.with(TemporalAdjusters.lastDayOfMonth());
        LocalDateTime tglDiskonAkhirBulan = akhirBulan.minusDays(3l);
        System.out.println("Tanggal akhir bulan: " + akhirBulan);
        System.out.println("Tanggal diskon akhir bulan: " + tglDiskonAkhirBulan);

        // edit data produk
        Produk p2 = databaseProduk.cariByCode("P-002");
        p2.setName("Laptop HP");
        p2.setPrice(new BigDecimal("11500000"));

        // tambahkan data produk baru
        Produk p5 = new Produk();
        p5.setCode("P-005");
        p5.setName("Handphone Oppo");
        p5.setPrice(new BigDecimal("5500000"));
        databaseProduk.semuaProduk().add(p5);

        // tulis data ke file
        databaseProduk.save();
    }
}
