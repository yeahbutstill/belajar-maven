package com.muhardin.endy.training.belajar.toko.ui;

import com.muhardin.endy.training.belajar.toko.dao.ProdukDb;
import com.muhardin.endy.training.belajar.toko.entity.Produk;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ProdukUI {
    private ProdukDb db;
    private List<Produk> dataProduk;
    private JTextField txtKode;
    private JTextField txtNama;
    private JFormattedTextField txtHarga;
    private JTable tblProduk;

    public static void main(String[] args) throws Exception {
        new ProdukUI().tampilkanAplikasi();
    }

    public void tampilkanAplikasi() throws Exception {
        JFrame fr = new JFrame();
        fr.setTitle("Aplikasi Toko");
        fr.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        fr.getContentPane().setLayout(new BorderLayout());

        JPanel pnlButton = siapkanPanelAtas();
        fr.getContentPane().add(pnlButton, BorderLayout.PAGE_START);

        JPanel pnlInput = siapkanFormInput();
        fr.getContentPane().add(pnlInput, BorderLayout.LINE_END);

        db = new ProdukDb();
        dataProduk = db.semuaProduk();

        tblProduk = new JTable(new ProdukTableModel(dataProduk));
        tblProduk.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblProduk.getSelectionModel().addListSelectionListener(new TabelProdukListener(this));

        fr.getContentPane().add(new JScrollPane(tblProduk), BorderLayout.CENTER);

    /*
    // Border Layout
    fr.getContentPane().setLayout(new BorderLayout());
    fr.getContentPane().add(btnTambah, BorderLayout.PAGE_START);
    fr.getContentPane().add(btnEdit, BorderLayout.PAGE_END);
    fr.getContentPane().add(btnHapus, BorderLayout.LINE_START);
    fr.getContentPane().add(btnSimpan, BorderLayout.LINE_END);
    fr.getContentPane().add(btnUlang);
     */

        // Flow Layout
        // fr.getContentPane().setLayout(new FlowLayout());

        // Grid Layout
        // fr.getContentPane().setLayout(new GridLayout(3,2));

        // BoxLayout
    /*
    fr.getContentPane().setLayout(new BoxLayout(fr.getContentPane(), BoxLayout.PAGE_AXIS));
    fr.getContentPane().add(btnTambah);
    fr.getContentPane().add(btnEdit);
    fr.getContentPane().add(btnHapus);
    fr.getContentPane().add(btnSimpan);
    fr.getContentPane().add(btnUlang);
     */

        // fr.setSize(800, 600);
        fr.pack();
        fr.setLocationRelativeTo(null);
        fr.setVisible(true);
    }

    private void setFormEditable(Boolean editable) {
        txtKode.setEditable(editable);
        txtNama.setEditable(editable);
        txtHarga.setEditable(editable);
    }

    private void kosongkanForm() {
        txtKode.setText("");
        txtNama.setText("");
        txtHarga.setText("");
        setFormEditable(false);
    }

    public void isiForm(Produk p) {
        txtKode.setText(p.getCode());
        txtNama.setText(p.getName());
        txtHarga.setValue(p.getPrice());
    }

    /**
     * ini nanti si table model akan mengecek lagi datanya dia, apakah ada data berubahan. nanti dia akan menyesuaikan dengan data yang terbaru.
     */
    public void refreshTabel() {
        ((ProdukTableModel) tblProduk.getModel()).fireTableDataChanged();
    }

    private JPanel siapkanFormInput() {
        // textfield
        txtKode = new JTextField(20);
        txtNama = new JTextField(20);
        txtHarga = new JFormattedTextField(NumberFormat.getCurrencyInstance(Locale.of("in", "id")));
        txtHarga.setColumns(20);
        txtHarga.setValue(BigDecimal.ZERO);

        JPanel pnlInput = new JPanel();
        pnlInput.setBorder(BorderFactory.createTitledBorder("Input Produk"));
        pnlInput.setLayout(new GridBagLayout());

        GridBagConstraints g1 = new GridBagConstraints();
        g1.gridx = 0;
        g1.gridy = 0;
        g1.weightx = 0.1;
        g1.fill = GridBagConstraints.HORIZONTAL;
        pnlInput.add(new JLabel("Kode"), g1);

        GridBagConstraints g2 = new GridBagConstraints();
        g2.gridx = 0;
        g2.gridy = 1;
        pnlInput.add(new JLabel("Nama"), g2);

        GridBagConstraints g3 = new GridBagConstraints();
        g3.gridx = 0;
        g3.gridy = 2;
        pnlInput.add(new JLabel("Harga"), g3);

        GridBagConstraints g4 = new GridBagConstraints();
        g4.gridx = 1;
        g4.gridy = 0;
        pnlInput.add(txtKode, g4);

        GridBagConstraints g5 = new GridBagConstraints();
        g5.gridx = 1;
        g5.gridy = 1;
        pnlInput.add(txtNama, g5);

        GridBagConstraints g6 = new GridBagConstraints();
        g6.gridx = 1;
        g6.gridy = 2;
        pnlInput.add(txtHarga, g6);

        setFormEditable(false);

        return pnlInput;
    }

    private JPanel siapkanPanelAtas() {
        // button
        JButton btnTambah = new JButton("Tambah");
        JButton btnEdit = new JButton("Edit");
        JButton btnHapus = new JButton("Hapus");
        JButton btnSimpan = new JButton("Simpan");
        JButton btnUlang = new JButton("Ulang");

        // action listener
        btnTambah.addActionListener(new ActionListener() { // anonymous innerclass

            @Override
            public void actionPerformed(ActionEvent e) {
                kosongkanForm();
            }
        });

        btnEdit.addActionListener(new ActionListener() { // anonymous innerclass

            @Override
            public void actionPerformed(ActionEvent e) {
                setFormEditable(true);
            }
        });

        btnHapus.addActionListener(new ActionListener() { // anonymous innerclass

            @Override
            public void actionPerformed(ActionEvent e) {
                // ambil dahulu indexnya
                int index = tblProduk.getSelectedRow();
                // jika index sama denagan -1 nah berarti tidak ada yang dipilih
                if (index == -1) {
                    JOptionPane.showMessageDialog(null, "Produk belum dipilih", "Error", JOptionPane.ERROR_MESSAGE);
                }

                // kalau ada yang dipilih maka...
                db.hapus(index);
                // handle UI
                ProdukUI.this.kosongkanForm();
                ProdukUI.this.setFormEditable(false);
                ProdukUI.this.refreshTabel();
            }
        });

        btnSimpan.addActionListener(new ActionListener() { // anonymous innerclass

            @Override
            public void actionPerformed(ActionEvent e) {
                // kita ambil dulu produknya
                int selectedIndex = tblProduk.getSelectedRow();
                Produk p = new Produk();
                if (selectedIndex != -1) {
                    p = dataProduk.get(selectedIndex);
                }

                p.setCode(txtKode.getText());
                p.setName(txtNama.getText());
                p.setPrice(new BigDecimal((Long) txtHarga.getValue()));

                // jadi ini kalau tidak ada yang dipilih, berarti record baru
                if (selectedIndex == -1) {
                    // nah kalau record baru kita add
                    dataProduk.add(p);
                }

                db.save();
                refreshTabel();
                kosongkanForm();
                setFormEditable(false);
            }
        });

        JPanel pnlButton = new JPanel();
        pnlButton.setLayout(new GridBagLayout());

        GridBagConstraints gp1 = new GridBagConstraints();
        gp1.gridx = 0;
        gp1.gridy = 0;
        gp1.weightx = 0.1;
        gp1.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnTambah, gp1);

        GridBagConstraints gp2 = new GridBagConstraints();
        gp2.gridx = 1;
        gp2.gridy = 0;
        gp2.weightx = 0.1;
        gp2.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnEdit, gp2);

        GridBagConstraints gp3 = new GridBagConstraints();
        gp3.gridx = 2;
        gp3.gridy = 0;
        gp3.weightx = 0.1;
        gp3.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnHapus, gp3);

        GridBagConstraints gp4 = new GridBagConstraints();
        gp4.gridx = 3;
        gp4.gridy = 0;
        gp4.weightx = 0.1;
        gp4.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnSimpan, gp4);

        GridBagConstraints gp5 = new GridBagConstraints();
        gp5.gridx = 4;
        gp5.gridy = 0;
        gp5.weightx = 0.1;
        gp5.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnUlang, gp5);
        return pnlButton;
    }

    // innerclass A.K.A class didalam class
    class TabelProdukListener implements ListSelectionListener {

        private ProdukUI produkUi;

        public TabelProdukListener(ProdukUI produkUi) {
            this.produkUi = produkUi;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel) e.getSource();
            // kalau kosong jangan diisi form
            if (lsm.getMinSelectionIndex() != -1) {
                produkUi.isiForm(dataProduk.get(lsm.getMinSelectionIndex()));
            }
        }
    }
}
