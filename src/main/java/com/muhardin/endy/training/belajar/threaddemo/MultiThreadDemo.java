package com.muhardin.endy.training.belajar.threaddemo;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        int jumlahThread = 5;
        List<CounterDemo> daftarThread = new ArrayList<>();

        for (int i = 0; i < jumlahThread; i++) {
            CounterDemo counterDemo = new CounterDemo();
            daftarThread.add(counterDemo);
            counterDemo.start();
        }

        // panggil join di semua thread
        for (CounterDemo cd : daftarThread) {
            cd.join();
        }

        System.out.println("Semua thread telah selesai");
    }

    static class CounterDemo extends Thread {
        private Counter counter = new Counter();

        @Override
        public void run() {
            try {
                counter.hitung();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
