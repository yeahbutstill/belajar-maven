package com.muhardin.endy.training.belajar.threaddemo;

public class Counter {
    private Integer angka = 0;
    private Integer ulang = 10;

    public void hitung() throws InterruptedException {
        for (int i = 0; i < ulang; i++) {
            angka++;
            System.out.println(Thread.currentThread().getName() + ": " + angka);
            Thread.sleep(1 * 1000);
        }
    }

    public void tambah() {
        angka++;
        show();
    }

    public void kurang() {
        angka--;
        show();
    }

    public void show() {
        System.out.println("Angka saat ini: " + angka);
    }
}
