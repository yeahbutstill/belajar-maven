package com.muhardin.endy.training.belajar.toko.service.impl;

import com.muhardin.endy.training.belajar.toko.entity.Pembelian;
import com.muhardin.endy.training.belajar.toko.service.Diskon;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

public class DiskonAkhirBulan implements Diskon {
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.2);

    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        LocalDateTime akhirBulan = LocalDateTime.now().with(TemporalAdjusters.lastDayOfMonth());
        LocalDateTime tigaHariSebelumAkhirBulan = akhirBulan.minusDays(3L);
        if (pembelian.getWaktuTransaksi().isAfter(tigaHariSebelumAkhirBulan)
                && pembelian.getWaktuTransaksi().isBefore(akhirBulan)) {
            return PERSENTASE_DISKON.multiply(pembelian.total());
        }

        // else
        return BigDecimal.ZERO;
    }
}
