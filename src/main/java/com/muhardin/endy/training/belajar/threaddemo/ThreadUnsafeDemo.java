package com.muhardin.endy.training.belajar.threaddemo;

import java.util.ArrayList;
import java.util.List;

public class ThreadUnsafeDemo {

    public static void main(String[] args) throws InterruptedException {
        Integer jumlahThread = 1;
        Integer jumlahUlang = 10;
        Counter counter = new Counter();

        List<TambahCounter> daftarThreadTambah = new ArrayList<>();
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadTambah.add(new TambahCounter(counter, jumlahUlang));
        }

        List<KurangCounter> daftarThreadKurang = new ArrayList<>();
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadKurang.add(new KurangCounter(counter, jumlahUlang));
        }

        // start semua thread
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadTambah.get(i).start();
            daftarThreadKurang.get(i).start();
        }

        // join semua thread
        for (int i = 0; i < jumlahThread; i++) {
            daftarThreadTambah.get(i).join();
            daftarThreadKurang.get(i).join();
        }

        // tampilkan
        System.out.println("========= Yang paling akhir ==========");
        counter.show();
    }

    static class TambahCounter extends Thread {
        private Integer ulang;
        private Counter counter;

        private TambahCounter(Counter c, Integer u) {
            this.counter = c;
            this.ulang = u;
        }

        @Override
        public void run() {
            for (int i = 0; i < ulang; i++) {
                counter.tambah();
            }
        }
    }

    static class KurangCounter extends Thread {
        private Integer ulang;
        private Counter counter;

        private KurangCounter(Counter c, Integer u) {
            this.counter = c;
            this.ulang = u;
        }

        @Override
        public void run() {
            for (int i = 0; i < ulang; i++) {
                counter.kurang();
            }
        }
    }
}
