package com.muhardin.endy.training.belajar.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ChatServerMultiThread {
    private Integer port;
    private Boolean running = true;

    public ChatServerMultiThread(Integer port) {
        this.port = port;
    }

    public void start() {
        System.out.println("Menunggu koneksi di port: " + port);

        // Input Stream dan Output Stream untuk bertukar data
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            List<Thread> daftarKoneksi = new ArrayList<>();
            while (running) {
                // pada waktu dia accept, dia akan mendapatkan socket, lalu si socket ini akan dilemparkan
                // ke thread baru
                Socket socket = serverSocket.accept(); // Thread baru
                System.out.println("Running: " + running);
                // new ClientHandler(socket).start(); // ini kalau extends Thread

                // ini kalau implement Runnable
                Thread t = new Thread(new ClientHandler(socket));
                daftarKoneksi.add(t);
                t.start();
            }
            System.out.println("Selesai menerima koneksi");

            // menunggu semua thread selesai
            for (Thread thd : daftarKoneksi) {
                thd.join();
            }

            // setelah semua selesai, baru jalankan printah berikut
            System.out.println("Semua koneksi sudah ditutup");

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        } catch (InterruptedException e) {
            System.err.format("InterruptedException: %s%n", e);
        }
    }

    // inner class
    // jadi kalau kita extends Thread maka untuk bisa menjalankan di thread yang terpisah kita harus
    // override method tersebut
    class ClientHandler implements Runnable {
        // lalu disini kita butuh instance variable
        private Socket socket;

        // lalu kita bikin constructornya
        public ClientHandler(Socket socket) {
            this.socket = socket;
            System.out.println("Menerima client di thread " + Thread.currentThread().getName());
        }

        @Override
        public void run() {
            // bedanya disini kita tidak listen, kita langsung dapat socket
            try (BufferedReader reader =
                         new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 PrintWriter writer =
                         new PrintWriter(socket.getOutputStream(), true, StandardCharsets.UTF_8)) {

                System.out.println("Ada client connect dari IP: " + socket.getInetAddress());

                // handle message dari client
                String data;
                while ((data = reader.readLine()) != null) {
                    String reply = data.toUpperCase();
                    System.out.println("C>" + data);
                    writer.println(reply);

                    // ketika mengetik stop maka dia tidak akan menerima koneksi berikutnya
                    if ("stop".equalsIgnoreCase(data)) {
                        // mengakses variable dari luar class
                        ChatServerMultiThread.this.running = false;
                    }

                    if (":q".equalsIgnoreCase(data)) {
                        break;
                    }
                }
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
        }
    }
}
