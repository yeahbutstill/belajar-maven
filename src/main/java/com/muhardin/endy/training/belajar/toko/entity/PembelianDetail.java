package com.muhardin.endy.training.belajar.toko.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PembelianDetail {
    private Produk produk;
    private Integer jumlah;

    /**
     * default constructor, otomatis dibuatkan oleh Java SDK. Kalau kita tidak membuat constructor
     * sendiri
     */
    //  public PembelianDetail() {
    //
    //  }

    /**
     * kalau sudah buat constructor, maka default constructor tidak akan dibuatkan otomatis
     *
     * @param p
     * @param jumlah
     */
    public PembelianDetail(Produk p, Integer jumlah) {
        produk = p;
        this.jumlah = jumlah;
    }

    public BigDecimal subTotal() {
        return produk.getPrice().multiply(new BigDecimal(jumlah));
    }
}
