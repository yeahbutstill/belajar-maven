package com.muhardin.endy.training.belajar.theconditionaloperators;

public class ConditionalDemo1 {
    public static void main(String[] args) {
        int value1 = 1;
        int value2 = 2;
        // harus dua-duanya true
        if (value1 == 1 && value2 == 2) System.out.println("value1 is 1 AND value2 is 2");
        // boleh salah satu true
        if (value1 == 1 || value2 == 1) System.out.println("value1 is 1 OR value2 is 1");
    }
}
