package com.muhardin.endy.training.belajar.toko.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

public class FormatAngkaHelper {
    private FormatAngkaHelper() {
        throw new IllegalStateException("FormatAngkaHelper");
    }

    public static String formatAngkaUang(BigDecimal angka) {
        return NumberFormat.getCurrencyInstance(Locale.of("id", "id"))
                .format(angka.setScale(0, RoundingMode.HALF_EVEN));
    }
}
