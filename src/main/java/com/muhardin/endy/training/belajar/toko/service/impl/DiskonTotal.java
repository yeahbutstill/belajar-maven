package com.muhardin.endy.training.belajar.toko.service.impl;

import com.muhardin.endy.training.belajar.toko.entity.Pembelian;
import com.muhardin.endy.training.belajar.toko.service.Diskon;

import java.math.BigDecimal;

public class DiskonTotal implements Diskon {

    private static final BigDecimal BATAS_DISKON = new BigDecimal(20000000);
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.10); // 10%

    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        if (BATAS_DISKON.compareTo(pembelian.total()) == -1) {
            return PERSENTASE_DISKON.multiply(pembelian.total());
        } else {
            return BigDecimal.ZERO;
        }
    }
}
