package com.muhardin.endy.training.belajar.samadengan;

import lombok.Getter;
import lombok.Setter;

public class SamaDenganVsEquals {
    public static void main(String[] args) {
        // primitive type, dia bukan object
        int value1 = 1;
        int value2 = 2;

        // not primitive type, dia object dan mempunyai method
        Integer val1 = new Integer(3);
        Integer val2 = new Integer(4);

        Value1 value11 = new Value1();
        value11.setNilai(10);

        Value2 value21 = new Value2();
        value21.setNilai(10);

        if (value1 == value2) System.out.println("valu1 == value2");
        if (value1 != value2) System.out.println("value1 != value2");
        if (value1 > value2) System.out.println("value1 > value2");
        if (value1 < value2) System.out.println("value1 < value2");
        if (value1 <= value2) System.out.println("value1 <= value2");
        if (value1 >= value2) System.out.println("value1 >= value2");

        System.out.println("=======================================");

        if (val1.equals(val2)) System.out.println("val1 == val2");
        if (!val1.equals(val2)) System.out.println("val1 != val2");
        if (val1 > val2) System.out.println("val1 > val2");
        if (val1 < val2) System.out.println("val1 < val2");
        if (val1 <= val2) System.out.println("val1 <= val2");
        if (val1 >= val2) System.out.println("val1 >= val2");

        System.out.println("=======================================");

        if (value11 == value21)
            System.out.println(
                    "jangan dipakai untuk membandingkan object, karena == ini dia membandingkan lokasi memory, bukan isinya");
        // pakai ini jika suatu object ingin dibandingkan isi nilainya
        if (value11.equals(value21)) System.out.println("sama");
        if (!value11.equals(value21)) System.out.println("tidak sama");
    }
}

@Getter
@Setter
class Value1 {
    Integer nilai;
}

class Value2 extends Value1 {
}
