package com.muhardin.endy.training.belajar.toko.entity;

import com.muhardin.endy.training.belajar.toko.service.Diskon;
import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Pembelian {
    private StatusTransaksi status = StatusTransaksi.BELUM_DIBAYAR;
    private LocalDateTime waktuTransaksi = LocalDateTime.now();
    private Customer customer;
    private List<PembelianDetail> daftarPembelianDetail = new ArrayList<>();
    private List<Diskon> daftarDiskon = new ArrayList<>();

    public BigDecimal total() {
        BigDecimal hasil = BigDecimal.ZERO;
        for (PembelianDetail pembelianDetail : daftarPembelianDetail) {
            hasil = hasil.add(pembelianDetail.subTotal());
        }
        return hasil;
    }

    public BigDecimal totalDiskon() {
        BigDecimal hasil = BigDecimal.ZERO;
        for (Diskon d : daftarDiskon) {
            hasil = hasil.add(d.hitung(this));
        }
        return hasil;
    }

    public BigDecimal totalBayar() {
        return total().subtract(totalDiskon());
    }

    public void tampilkanRincianDiskon() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.of("in", "id"));
        for (Diskon d : daftarDiskon) {
            String simpleName = d.getClass().getSimpleName();
            System.out.println(
                    simpleName + ": " + formatter.format(d.hitung(this).setScale(0, RoundingMode.HALF_EVEN)));
        }
    }
}
