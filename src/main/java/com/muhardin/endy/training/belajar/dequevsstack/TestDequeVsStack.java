package com.muhardin.endy.training.belajar.dequevsstack;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Stack;

public class TestDequeVsStack {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(1);
        stack.push(3);
        stack.push(4);
        System.out.println(new ArrayList<>(stack));

        Deque<Integer> deque = new ArrayDeque<>();
        deque.push(1);
        deque.push(2);
        deque.push(1);
        deque.push(3);
        deque.push(4);
        System.out.println(new ArrayList<>(deque));
    }
}
