package com.muhardin.endy.training.belajar.toko.entity;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Customer {
    private String name;
    private String email;
}
