package com.muhardin.endy.training.belajar.concurrency;

public class Counter {
    private Integer x = 0;

    public void increment() {
        this.x++;
    }

    public void decrement() {
        this.x--;
    }

    public void show() {
        System.out.println("Nilai X : " + x);
    }
}
